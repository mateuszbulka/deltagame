package com.example.mateusz.deltagame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;

public class ShipView extends View
{
    private Bitmap ship[] = new Bitmap[2];
    private int shipX = 10;
    private int shipY;
    private int shipSpeed;

    private int canvasWidth, canvasHeight;

    private int yellowX, yellowY, yellowSpeed = 10;
    private Bitmap yellow;

    private int greenX, greenY, greenSpeed = 12;
    private Bitmap green;

    private int redX, redY, redSpeed = 12;
    private Bitmap red;


    private int score;

    private boolean touch = false;

    private Bitmap backgroundImage;

    private Paint scorePaint = new Paint();

    private Bitmap life[] = new Bitmap[2];

    public ShipView(Context context) {
        super(context);

        ship[0] = BitmapFactory.decodeResource(getResources(), R.drawable.ship1);
        ship[1] = BitmapFactory.decodeResource(getResources(), R.drawable.ship2);

        backgroundImage = BitmapFactory.decodeResource(getResources(), R.drawable.background);

        yellow = BitmapFactory.decodeResource(getResources(), R.drawable.yellow);
        green = BitmapFactory.decodeResource(getResources(), R.drawable.green);
        red = BitmapFactory.decodeResource(getResources(), R.drawable.red);

        scorePaint.setColor(Color.WHITE);
        scorePaint.setTextSize(70);
        scorePaint.setTypeface(Typeface.DEFAULT_BOLD);
        scorePaint.setAntiAlias(true);

        life[0] = BitmapFactory.decodeResource(getResources(), R.drawable.hearts);
        life[1] = BitmapFactory.decodeResource(getResources(), R.drawable.heart_grey);

        shipY = 550;
        score = 0;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvasWidth = canvas.getWidth();
        canvasHeight = canvas.getHeight();

        canvas.drawBitmap(backgroundImage, 0, 0, null);

        int minShipY = 100;
        int maxShipY = canvasHeight - ship[0].getHeight();
        shipY = shipY + shipSpeed;

        if (shipY < minShipY)
        {
            shipY = minShipY;
        }
        if (shipY > maxShipY)
        {
            shipY = maxShipY;
        }
        shipSpeed = shipSpeed + 2;


        if(touch)
        {
            canvas.drawBitmap(ship[1], shipX, shipY, null);
            touch = false;
        }
        else
        {
            canvas.drawBitmap(ship[0], shipX, shipY, null);
        }

        //------------Sfera z celu-------//

        yellowX = yellowX - yellowSpeed;

        if(hitBallChecker(yellowX, yellowY))
        {
            score = score + 10;
            yellowX =  -100;
        }

        if (yellowX < 0)
        {
            yellowX = canvasWidth + 4000;
            yellowY = (int) Math.floor(Math.random() * (maxShipY - minShipY)) +  minShipY;
        }
        canvas.drawBitmap(yellow, yellowX, yellowY, null);

        //------------Przybornik naprawczy-------//

        greenX = greenX - greenSpeed;

        if(hitBallChecker(greenX, greenY))
        {
            score = score + 5;
            greenX =  -100;
        }

        if (greenX < 0)
        {
            greenX = canvasWidth + 5000;
            greenY = (int) Math.floor(Math.random() * (maxShipY - minShipY)) +  minShipY;
        }
        canvas.drawBitmap(green, greenX, greenY, null);

        //------------Przeciwnicy-------//

        redX = redX - redSpeed;

        if(hitBallChecker(redX, redY))
        {
            score = score - 15;
            redX =  -100;
        }

        if (redX < 0)
        {
            redX = canvasWidth + 15;
            redY = (int) Math.floor(Math.random() * (maxShipY - minShipY)) +  minShipY;
        }
        canvas.drawBitmap(red, redX, redY, null);

        canvas.drawText("Wynik : " + score, 20, 60, scorePaint);

        canvas.drawBitmap(life[0], 580, 10, null);
        canvas.drawBitmap(life[0], 680, 10, null);
        canvas.drawBitmap(life[0], 780, 10, null);
    }

    public boolean hitBallChecker(int x, int y)
    {
        if(shipX <= x && x <= (shipX + ship[0].getWidth()) && shipY <= y && y <= (shipY + ship[0].getHeight()))
        {
            return true;
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            touch = true;

            shipSpeed = -22;
        }
        return true;
    }
}
